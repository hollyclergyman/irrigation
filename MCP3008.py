#!/usr/bin/python3

import RPi.GPIO as GPIO
import time

# solution by:
# https://www.raspiprojekt.de/machen/basics/schaltungen/26-analoge-signale-mit-dem-mcp3008-verarbeiten.html?showall=1

GPIO.setmode(GPIO.BCM)

HIGH = True
LOW = False

def getAnalogData(adCh, CLKpin, DINpin, DOUTpin, CSPpin, sleep=0.5):
    GPIO.output(CSPpin,HIGH)
    GPIO.output(CSPpin,LOW)
    GPIO.output(CLKpin,LOW)

    cmd = adCh
    cmd |= 0x18
    cmd <<= 3

    for i in range(5):
        if (cmd & 0x10):
            GPIO.output(DINpin, HIGH)
        else:
            GPIO.output(DINpin, LOW)
        GPIO.output(CLKpin, HIGH)
        GPIO.output(CLKpin, LOW)
        cmd <<= 1

    addchvalue = 0
    for x in range(12):
        """
        This is now a range, which is executed on the analogue digital converter, 
        by which the clock of that converter is deployed. With each round of the loop,
        the clock is set again.
        """
        GPIO.output(CLKpin, HIGH)
        GPIO.output(CLKpin, LOW)
        addchvalue <<= 1
        # <<= bitwise operator for bit shifted to the left by the value of 1
        # https://wiki.python.org/moin/BitwiseOperators
        if (GPIO.input(DOUTpin)):
            addchvalue |= 0x1
            # the |= operator is a bit wise OR
            # which is moved over the input with the 
            # hexadecimal value 0x01
    GPIO.output(CSPpin, HIGH)
    time.sleep(sleep)
    addchvalue >>= 1
    return addchvalue

CH = 0
CLK = 18
DIN = 24
DOUT = 23
CS = 25

GPIO.setup(CLK, GPIO.OUT)
GPIO.setup(DIN, GPIO.OUT)
GPIO.setup(DOUT, GPIO.IN)
GPIO.setup(CS, GPIO.OUT)

def pinvalues(sleep=0.5):
    while True:
        yield getAnalogData(CH, CLK, DIN, DOUT, CS, sleep)


if __name__ == "__main__":
    try:
        while True:
            for x in pinvalues():
                print(x)
    except KeyboardInterrupt:
        print("Terminating")
